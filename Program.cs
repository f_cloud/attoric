﻿using System;
using System.IO;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using CsvHelper;
using System.Net;
using System.Globalization;
namespace attoric
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=localhost;userid=usr_wp@localhost;password=pippo_1993;database=analisi_programmazione;";
            using var con = new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"MySql Version:{con.ServerVersion}");
            DirectoryInfo d = new DirectoryInfo(@"/var/www/html/AttoriC/attoric"); //Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.csv"); //Getting Text files

            string str = "";

            foreach (FileInfo file in Files)

            {
                long contatore = 0;

                int idEmittente = 0;

                str = str + ", " + file.Name;
                var controlloFile = "SELECT nome_file FROM parsati WHERE nome_file = @str";
                Console.WriteLine(file.Name);
                using var cmd1 = new MySqlCommand(controlloFile, con);
                cmd1.Parameters.AddWithValue("@str", file.Name);
                cmd1.Prepare();
                try
                {
                    //cmd1.ExecuteQuery();
                    //using var cmdrec = new MySqlCommand(sqlrec, con);
                    using MySqlDataReader rdr = cmd1.ExecuteReader();
                    if (rdr.Read())
                    {
                        Console.WriteLine("Read: {0}", rdr.GetString(0));
                    }
                    else
                    {
                        rdr.Close();
                        var inserisci = "INSERT INTO parsati (nome_file) VALUES (@nome_file)";
                        using var cmd2 = new MySqlCommand(inserisci, con);
                        cmd2.Parameters.AddWithValue("@nome_file", file.Name);
                        cmd2.Prepare();
                        try
                        {
                            cmd2.ExecuteNonQuery();
                            var idFileParsato = cmd2.LastInsertedId;

                            Console.WriteLine("file aggiunto: " + idFileParsato);


                            if (file.Name != "BancaDati Audiovisivo.csv")

                            {
                                TextReader reader1 = new StreamReader(file.Name);
                                var csvReader1 = new CsvReader(reader1, new System.Globalization.CultureInfo("it-it"));
                                var trasmissionifromcsv = csvReader1.GetRecords<trasmissione>();
                                foreach (trasmissione trasmissionefromcsv in trasmissionifromcsv)
                                {
                                    var controlloEmittente = "SELECT id FROM emittente WHERE nome_emittente = @nome_emittente";
                                    using var cmd6 = new MySqlCommand(controlloEmittente, con);
                                    cmd6.Parameters.AddWithValue("@nome_emittente", trasmissionefromcsv.canale);
                                    cmd6.Prepare();
                                    try
                                    {
                                        using MySqlDataReader rdrEmittente = cmd6.ExecuteReader();
                                        if (rdrEmittente.Read())
                                        {

                                            idEmittente = rdrEmittente.GetInt32(0);

                                            //Console.WriteLine("Read: {0}", rdr.GetString(0));
                                        }
                                        else
                                        {
                                            rdrEmittente.Close();
                                            var inserisciEmittente = "INSERT INTO emittente (nome_emittente) VALUES (@nome_emittente)";
                                            using var cmd5 = new MySqlCommand(inserisciEmittente, con);
                                            cmd5.Parameters.AddWithValue("@nome_emittente", trasmissionefromcsv.canale);
                                            cmd5.Prepare();
                                            try
                                            {
                                                cmd5.ExecuteNonQuery();

                                                idEmittente = ((int)cmd5.LastInsertedId);

                                            }

                                            catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }

                                        }

                                    }

                                    catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }

                                    Console.WriteLine(" " + trasmissionefromcsv.titolo_opera + " " + trasmissionefromcsv.data);

                                    //Console.WriteLine("sto a fa qualcosa");

                                    var sql = "INSERT INTO trasmissione (id_canale, data, ora_inizio, ora_fine, titolo_opera, titolo_opera_originale, titolo_puntata,titolo_puntata_originale , numero_puntata, numero_stagione, regia, id_file_provenienza) VALUES (@id_canale, @data, @ora_inizio, @ora_fine, @titolo_opera, @titolo_opera_originale, @titolo_puntata,@titolo_puntata_originale , @numero_puntata, @numero_stagione, @regia, @id_file_provenienza)";
                                    using var cmd3 = new MySqlCommand(sql, con);
                                    cmd3.Parameters.AddWithValue("@id_canale", idEmittente);
                                    cmd3.Parameters.AddWithValue("@data", trasmissionefromcsv.data);
                                    cmd3.Parameters.AddWithValue("@ora_inizio", trasmissionefromcsv.ora_inizio);
                                    cmd3.Parameters.AddWithValue("@ora_fine", trasmissionefromcsv.ora_fine);
                                    cmd3.Parameters.AddWithValue("@titolo_opera", trasmissionefromcsv.titolo_opera);
                                    cmd3.Parameters.AddWithValue("@titolo_opera_originale", trasmissionefromcsv.titolo_opera_originale);
                                    cmd3.Parameters.AddWithValue("@titolo_puntata", trasmissionefromcsv.titolo_puntata);
                                    cmd3.Parameters.AddWithValue("@titolo_puntata_originale", trasmissionefromcsv.titolo_puntata_originale);
                                    cmd3.Parameters.AddWithValue("@numero_puntata", trasmissionefromcsv.numero_puntata);
                                    cmd3.Parameters.AddWithValue("@numero_stagione", trasmissionefromcsv.numero_stagione);
                                    cmd3.Parameters.AddWithValue("@regia", trasmissionefromcsv.regia);
                                    cmd3.Parameters.AddWithValue("@id_file_provenienza", idFileParsato);
                                    cmd3.Prepare();
                                    try
                                    {
                                        cmd3.ExecuteNonQuery();

                                        contatore++;

                                        Console.WriteLine("record aggiunto");

                                    }
                                    catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                                }

                            }
                            else
                            {
                              Console.WriteLine("parso BancaDati");
                                TextReader reader2 = new StreamReader(file.Name);
                                var csvReader2 = new CsvReader(reader2, new System.Globalization.CultureInfo("it-it"));
                                var bancadatifromcsv = csvReader2.GetRecords<bancaDati>();
                                foreach (bancaDati datifromcsv in bancadatifromcsv)
                                {
                                    if (datifromcsv.titolo_italiano != "" | datifromcsv.titolo_originale != "")
                                    {
                                        var inserimentoBancaDati = "INSERT INTO banca_dati (titolo_italiano, titolo_originale, numero_stagione, titolo_puntata, numero_puntata, artista_rasi_primari, artista_rasi_comprimari, artista_rasi_doppiatori_primari, artista_rasi_doppiatori_comprimari, regia, anno_pubblicazione, anno_produzione, tipologia_opera, tipo_trasmissione, paese_produzione) VALUES (@titolo_italiano, @titolo_originale, @numero_stagione, @titolo_puntata, @numero_puntata, @artista_rasi_primari, @artista_rasi_comprimari, @artista_rasi_doppiatori_primari, @artista_rasi_doppiatori_comprimari, @regia, @anno_pubblicazione, @anno_produzione, @tipologia_opera, @tipo_trasmissione, @paese_produzione)";
                                        using var cmd7 = new MySqlCommand(inserimentoBancaDati, con);
                                        cmd7.Parameters.AddWithValue("@titolo_italiano", datifromcsv.titolo_italiano);
                                        cmd7.Parameters.AddWithValue("@titolo_originale", datifromcsv.titolo_originale);
                                        cmd7.Parameters.AddWithValue("@numero_stagione", datifromcsv.numero_stagione);
                                        cmd7.Parameters.AddWithValue("@titolo_puntata", datifromcsv.titolo_puntata);
                                        cmd7.Parameters.AddWithValue("@numero_puntata", datifromcsv.numero_puntata);
                                        cmd7.Parameters.AddWithValue("@artista_rasi_primari", datifromcsv.artista_rasi_primari);
                                        cmd7.Parameters.AddWithValue("@artista_rasi_comprimari", datifromcsv.artista_rasi_comprimari);
                                        cmd7.Parameters.AddWithValue("@artista_rasi_doppiatori_primari", datifromcsv.artista_rasi_doppiatori_primari);
                                        cmd7.Parameters.AddWithValue("@artista_rasi_doppiatori_comprimari", datifromcsv.artista_rasi_doppiatori_comprimari);
                                        cmd7.Parameters.AddWithValue("@regia", datifromcsv.regia);
                                        cmd7.Parameters.AddWithValue("@anno_pubblicazione", datifromcsv.anno_pubblicazione);
                                        cmd7.Parameters.AddWithValue("@anno_produzione", datifromcsv.anno_produzione);
                                        cmd7.Parameters.AddWithValue("@tipologia_opera", datifromcsv.tipologia_opera);
                                        cmd7.Parameters.AddWithValue("@tipo_trasmissione", datifromcsv.tipo_trasmissione);
                                        cmd7.Parameters.AddWithValue("@paese_produzione", datifromcsv.paese_produzione);
                                        cmd7.Prepare();
                                        try
                                        {
                                            cmd7.ExecuteNonQuery();
                                            contatore++;
                                            Console.WriteLine("dato aggiunto in banca dati");
                                        }
                                        catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                                    }
                                    else { Console.WriteLine("riga vuota"); }
                                }
                            }
                            string updateFileParsato = "UPDATE parsati SET numero_righe = @numero_righe WHERE id = @idFileParsato";
                            using var cmd4 = new MySqlCommand(updateFileParsato, con);
                            cmd4.Parameters.AddWithValue("@numero_righe", contatore);
                            cmd4.Parameters.AddWithValue("@idFileParsato", idFileParsato);
                            cmd4.Prepare();
                            try
                            {
                                cmd4.ExecuteNonQuery();
                            }
                            catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
                        }
                        catch (Exception e)
                        { Console.WriteLine("errore" + e.ToString()); }
                    }
                }
                catch (Exception e) { Console.WriteLine("errore" + e.ToString()); }
            }
var url = "http://www.omdbapi.com/?i=tt3896198&apikey=43a3cbf5";//Paste ur url here  
            WebRequest request = HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string responseText = reader.ReadToEnd();
            Console.WriteLine(responseText);
        }
    }
    class trasmissione
    {
        public string canale { get; set; }
        public string data { get; set; }
        public string ora_inizio { get; set; }
        public string ora_fine { get; set; }
        public string titolo_opera { get; set; }
        public string titolo_opera_originale { get; set; }
        public string titolo_puntata { get; set; }
        public string titolo_puntata_originale { get; set; }
        public string numero_puntata { get; set; }
        public string numero_stagione { get; set; }
        public string regia { get; set; }
    }
    class bancaDati
    {
        public string titolo_italiano { get; set; }
        public string titolo_originale { get; set; }
        public string numero_stagione { get; set; }
        public string titolo_puntata { get; set; }
        public string numero_puntata { get; set; }
        public string artista_rasi_primari { get; set; }
        public string artista_rasi_comprimari { get; set; }
        public string artista_rasi_doppiatori_primari { get; set; }
        public string artista_rasi_doppiatori_comprimari { get; set; }
        public string regia { get; set; }
        public string anno_pubblicazione { get; set; }
        public string anno_produzione { get; set; }
        public string tipologia_opera { get; set; }
        public string tipo_trasmissione { get; set; }
        public string paese_produzione { get; set; }
    }
    class emittente
    {
        public string nome_emittente { get; set; }
        public int id_gruppo { get; set; }
        public string file_provenienza { get; set; }
    }
}